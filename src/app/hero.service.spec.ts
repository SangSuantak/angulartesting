// import { HeroService, Hero } from "./hero.service";
// import { Observable } from "rxjs";

// // let httpClientSpy: {
// //     get: jasmine.Spy
// // }

// // let heroService: HeroService;

// // beforeEach(() => {
// //     httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
// //     heroService = new HeroService();
// // });
// const heroes: Hero[] = [
//     {
//         name: 'Sang',
//         age: 31
//     },
//     {
//         name: 'Neu',
//         age: 32
//     },
//     {
//         name: 'Moi',
//         age: 33
//     },
//     {
//         name: 'Lian',
//         age: 34
//     }
// ];

// describe('HeroService without angular support', () =>{
//     let heroService: HeroService;

//     it('Should return array of Observable Hero', (done: DoneFn) => {
//         heroService = new HeroService();
//         heroService.getHeroes().subscribe(values => {
//             expect(values).toBe(heroes);
//         });
//         //expect(heroService.getHeroes()).toBe(new Observable<Hero[]>());
//     });
// });