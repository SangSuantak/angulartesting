import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";

export class Hero {
    name: string = '';
    age: number;
}

const heroes: Hero[] = [
    {
        name: 'Sang',
        age: 31
    },
    {
        name: 'Neu',
        age: 32
    },
    {
        name: 'Moi',
        age: 33
    },
    {
        name: 'Lian',
        age: 34
    }
];

@Injectable()
export class HeroService {
    getHeroes(): Observable<Hero[]> {
        return of(heroes);
    }
}